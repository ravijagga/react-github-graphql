import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import DisplayRepos from './components/display-repos';
import UsernameForm from './components/username-form';

// Set Base URL & Headers
const axiosGitHubGraphQL = axios.create({
  baseURL: 'https://api.github.com/graphql',
  headers: {
    Authorization: 'bearer 705173fdb31ea02361fb0e2ed58fd7cef2ca8e1d',
  },
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      repositories: {}
    };
    this.setUsername = this.setUsername.bind(this);
  }

  setUserData(username) {
    // GraphQL Query
    const GET_USER_REPOS = `{
      user(login: "${username}") {
        name
        url
        email
        avatarUrl(size: 100)
        repositories(first: 10) {
          edges {
            node {
              id
              name
              url
              createdAt
              description
              forkCount
            }
          }
        }
      }
    }`;

    // Fetch user repos
    axiosGitHubGraphQL
      .post('', { query: GET_USER_REPOS })
      .then(result => {
        this.setState({ repositories: result.data.data.user });
      }).catch(err => {
        console.log('err', err);
      });
  }

  setUsername(musername) {
    if (musername !== this.state.username) {
      this.setState({
        username: musername
      });
      this.setUserData(musername);
    }
  }

  render() {
    if (!this.state.username) {
      return (
        <div>
          <UsernameForm setUsername={this.setUsername}/>
        </div>
      );
    } else {
      return (
        <div>
          <UsernameForm setUsername={this.setUsername}/>
          <DisplayRepos username={this.state.username} repositories={this.state.repositories}/>
        </div>
      );
    }
  }

}

export default App;