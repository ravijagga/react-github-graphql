import React, { Component } from 'react';

class UsernameForm extends Component {
  constructor(props) {
    super();
    this.state = {
      username: ''
    };
    this.setUsername = this.setUsername.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  setUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.setUsername(this.state.username);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <input type="text" name="username" onChange={this.setUsername}/>
        <input type="submit" value="submit"/>
      </form>
    );
  }
}

export default UsernameForm;