import React, { Component } from 'react';
import { AutoSizer, List } from 'react-virtualized';

class DisplayRepos extends Component {

  constructor(props) {
    super();
    this.rowRenderer = this.rowRenderer.bind(this);
  }

  rowRenderer({
    key,
    index,
    isScrolling,
    isVisible,
    style
  }) {
    return (
      <div key={key} style={style}>
        <h3>{this.props.repositories.repositories.edges[index].node.name}</h3>
        <ul>
          <li>
            Url: 
            <a href={this.props.repositories.repositories.edges[index].node.url} target="_blank">
              {this.props.repositories.repositories.edges[index].node.url}
            </a>
          </li>
          <li>description: {this.props.repositories.repositories.edges[index].node.description}</li>
          <li>forkCount: {this.props.repositories.repositories.edges[index].node.forkCount}</li>
          <li>createdAt: {this.props.repositories.repositories.edges[index].node.createdAt}</li>
        </ul>
      </div>
    );
  }

  render() {
    if (!this.props.repositories) {
      return <div>Not Found!</div>;
    } else {
      return (
        <div>
          <img src={this.props.repositories.avatarUrl}/>
          <ul>
            <li>User: { this.props.repositories.name }</li>
            <li>URL: {this.props.repositories.url}</li>
            <li>Email: {this.props.repositories.email}</li>
          </ul>

          <h2>Repositories:-</h2>
          <AutoSizer>
            {({ width }) => (
              <List
                width={width}
                height={280}
                rowCount={this.props.repositories.repositories ? this.props.repositories.repositories.edges.length : 0 }
                rowHeight={150}
                rowRenderer = {this.rowRenderer}
              />
            )}
          </AutoSizer>
        </div >
      );
    }
  }
}

export default DisplayRepos;